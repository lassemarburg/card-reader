# Card Reader

## Requirements

MFRC522 library

For MQTT Setup:

PuSubClient

## Setup

For the MQTT version replace WiFi credentials in *CardReaderMQTT.ino*

```c++
const char* ssid =  "yourSSID"; //use your ssid
const char* password = "yourPassword"; // use your password
```

And if need be the MQTT Broker URL

```c++
const char* mqtt_server = "test.mosquitto.org"; // use your MQTT Broker URL
```

Upload the sketch to your board.

## MQTT Topics and Messages

### From Card Reader

Every 5 Seconds

`devices/rfid/heartbeat`

On Card Present

`devices/rfid/card XXXXX` XXXXX is the Card ID

On Card Removed/No Card present

`devices/rfid/card none`

